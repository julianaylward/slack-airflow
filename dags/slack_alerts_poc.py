#https://medium.com/@xnuinside/short-guide-how-to-use-postgresoperator-in-apache-airflow-ca78d35fb435
#https://towardsdatascience.com/integrating-docker-airflow-with-slack-to-get-daily-reporting-c462e7c8828a

import uuid
from datetime import datetime, timedelta
import time
from airflow import DAG
from airflow.operators.postgres_operator import PostgresOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.utils.trigger_rule import TriggerRule
from airflow.utils.dates import days_ago
from airflow.models import Variable
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator


#Default args
args  = {
    'owner' : 'julian.aylward',
    'email': ['julian.aylward@majestic.co.uk'],
    'start_date': days_ago(1),
    'provide_context': True,
    'schedule_interval': '10 10 * * *'
}

#reference Airflow variable that locates any SQL files we need to pull in:
tmpl_search_path = Variable.get("sql_path")

#Set up a DAG
dag = DAG(
    dag_id='slack_alerts_poc',
    dagrun_timeout=timedelta(minutes=60),
    template_searchpath=tmpl_search_path,
    default_args=args,
    max_active_runs=1)

#Initiate with a dummy operator to denote dag commence:
dummy_start = DummyOperator(
    task_id = 'start_dag',
    dag = dag
    )

#Slack Webhook to notify Majestic BI Team Slack Workspace that the dag is beginning:
slack_start = SlackWebhookOperator(
        task_id = 'slack_start_dag',
        http_conn_id = 'slack_connection',
        message =  'Hello from Airflow! Dag run DWH Test commencing at UTC: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
        channel = '#airflow-monitoring',
        dag = dag
    )

# drop table (arbitrary)
drop_table_if = PostgresOperator(
    postgres_conn_id= 'original_dwh',
    task_id='drop_table_if_exists',
    sql='''DROP TABLE IF EXISTS majestic_analyst_adh.jca_new_table;''',
    dag = dag
    )

#create table
create_table = PostgresOperator(
    postgres_conn_id= 'original_dwh',
    task_id='create_table',
    sql='''CREATE TABLE majestic_analyst_adh.jca_new_table(
    custom_id integer NOT NULL, timestamp TIMESTAMP NOT NULL, user_id VARCHAR (50) NOT NULL
    );''',
    trigger_rule=TriggerRule.ALL_DONE,
    dag = dag
    )

#insert random row
insert_row = PostgresOperator(
    postgres_conn_id= 'original_dwh',
    task_id='insert_row',
    sql='INSERT INTO majestic_analyst_adh.jca_new_table VALUES(%s, %s, %s)',
    trigger_rule=TriggerRule.ALL_DONE,
    parameters=(uuid.uuid4().int % 123456789, datetime.now(), uuid.uuid4().hex[:10]),
    dag = dag
    )


#Slack Webhook to notify Majestic BI Team Slack Workspace that the dag is ending:
# Backslash is line continuation
slack_end = SlackWebhookOperator(
        task_id = 'slack_end_dag',
        http_conn_id = 'slack_connection',
        message =  'Hello from Airflow! Dag run DWH Test ending at UTC: {}'.format( \
        datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
        channel = '#airflow-monitoring',
        dag = dag
    )

dummy_end = DummyOperator(
    task_id = 'end_dag',
    dag = dag
    )


dummy_start >> slack_start
slack_start >> drop_table_if
drop_table_if >> create_table
create_table >> insert_row
insert_row >> slack_end
slack_end >> dummy_end


