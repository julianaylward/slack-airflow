#https://towardsdatascience.com/integrating-docker-airflow-with-slack-to-get-daily-reporting-c462e7c8828a

import os
import requests
from dotenv import load_dotenv, find_dotenv

from airflow import DAG
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from datetime import datetime

default_args = {
    'owner':'julian.aylward',
    'depends_on_past':False,
    'start_date': datetime(2020,7,13),
    'retries':0,
}

def get_daily_forecast() -> str:
    dotenv_path = find_dotenv()
    load_dotenv(dotenv_path)
    api_key = os.environ['OPEN_WEATHER_API']
    r = requests.get('http://api.openweathermap.org/data/2.5/weather?q=London&APPID={}'.format(api_key))
    forecast = r.json()['weather'][0]['description']
    return 'The forecast today for London is: {}'.format(forecast)


with DAG(
    'Weather_env_test',
    default_args = default_args,
    schedule_interval='15 7 * * *',
    catchup = False,
) as dag:
    post_daily_forecast = SlackWebhookOperator(
        task_id = 'post_daily_forecast',
        http_conn_id = 'slack_connection',
        message = get_daily_forecast(), #'Hello from Airflow!'
        channel = '#airflow-monitoring'
    )
