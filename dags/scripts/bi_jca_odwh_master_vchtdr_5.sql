-- ******************* START OF VOUCHER TRADING REPORT **********************
-- ******************* START OF VOUCHER TRADING REPORT  **********************


-- Archive master voucher tracker data in a table that doesn't get deleted and is an update/insert
-- This will ensure we always keep a record of vouchers / promocodes run even if there are issues with the Google Sheet or the upload process.


insert into majestic_analyst_adh.jca_master_voucher_tracker_1_archive
select a.*,
current_date as data_insert_date
from majestic_analyst_adh.jca_master_voucher_data_1 a
where unique_id not in (select distinct unique_id from majestic_analyst_adh.jca_master_voucher_data_1)
;


---- 1.) FIRST GET ALL STORE VOUCHER REDEMPTION ORDER IDS
drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_1;

create table majestic_analyst_adh.jca_all_vouchers_redeemed_1 as
select v.voucher_barcode, v.voucher_description, v.order_id, sum(v.redeemed_value) as redeemed_value
from warehouse.d_vouchers_redeemed_new v
where extract (year from v.accounting_year) >= (select comparison_year - 2 from tableau.current_accounting_dates)
and v.order_id is not null
group by v.voucher_barcode, v.voucher_description, v.order_id
;


---- 2.) JOIN ONTO D_ORDERS TO GET ORDER METRICS

drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_2;

create table majestic_analyst_adh.jca_all_vouchers_redeemed_2 as
select a.voucher_barcode, a.voucher_description, a.order_id, a.redeemed_value,
b.customer_status, b.customer_code, b.first_order, b.order_type,
b.store_code, b.web_order,
b.accounting_year, b.accounting_period, b.accounting_week, b.order_date,
b.sale_price_ex_vat, b.gross_profit, b.basket_quantity_total
from majestic_analyst_adh.jca_all_vouchers_redeemed_1 a
inner join warehouse.d_orders_new b
on a.order_id = b.order_id;


---- 2a.) GET ONLINE CODES AND REASON CODES TOO
---- 2a.) GET ONLINE CODES AND REASON CODES TOO
drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_2a;

create table majestic_analyst_adh.jca_all_vouchers_redeemed_2a as
select voucher_barcode, voucher_description, order_id, redeemed_value,
customer_status, customer_code, first_order, order_type,
store_code, web_order,
accounting_year, accounting_period, accounting_week, order_date,
sale_price_ex_vat, gross_profit, basket_quantity_total
from majestic_analyst_adh.jca_all_vouchers_redeemed_2
union all
select trim(voucher_code) as voucher_barcode, 'online code' as voucher_description,   -- some online codes have a blank space afterwards - unsure why!
order_id, voucher_amount as redeemed_value, customer_status, customer_code, first_order, order_type,
store_code, web_order,
accounting_year, accounting_period, accounting_week, order_date,
sale_price_ex_vat, gross_profit, basket_quantity_total
from warehouse.d_orders_new
where extract (year from accounting_year) >= (select comparison_year - 2 from tableau.current_accounting_dates)
and voucher_code is not null
-- and trim(voucher_code) not in (select online_code from majestic_analyst_adh.jca_master_voucher_data_1 where types = 'Unique Code')
and len(trim(voucher_code)) <= 3 -- if we ever use prefixes longer than 3 digits this will break. Needed to filter out prefixes from online codes
union all
select io.discount_reason_code::varchar as voucher_barcode, 'reason code' as voucher_description,
op.order_id, sum(total_discount_value) as voucher_value, op.customer_status, op.customer_code, op.first_order, op.order_type,
op.store_code, op.web_order,
op.accounting_year, op.accounting_period, op.accounting_week, op.transaction_date as order_date,
sum (op.sale_price_ex_vat) as sale_price_ex_vat, sum (op.gross_profit) as gross_profit,
sum (op.quantity) as basket_quantity_total
from warehouse.d_invoice_offers_new io inner join warehouse.d_order_products_new op
on io.invoice_number = op.invoice_number and io.line_no = op.line_no
and io.product_code = op.product_code and io.customer_code = op.customer_code
--inner join majestic_analyst_adh.jca_master_voucher_data_1 vd
--on op.transaction_date between vd.start_date and vd.expiry_date
WHERE io.discount_reason_code in
(select reason_code from
majestic_analyst_adh.jca_master_voucher_data_1
where types = 'Reason code')
and op.order_type <> 'Refund'
GROUP BY io.discount_reason_code, op.order_id, op.customer_status,
op.customer_code, op.first_order, op.order_type, op.store_code, op.web_order,
op.accounting_year, op.accounting_period, op.accounting_week, op.transaction_date
union all
-- Added by Alex Sherriff December 2019 to include the reason codes that are generated from New Till Data.
select
a.discount_description::varchar as voucher_barcode,
'reason code' as voucher_description,
b.order_id,
sum(a.redeemed_value) as voucher_value,
b.customer_status,
b.customer_code,
b.first_order,
b.order_type,
b.store_code,
b.web_order,
b.accounting_year,
b.accounting_period,
b.accounting_week,
b.transaction_date as order_date,
sum(b.sale_price_ex_vat) as sale_price_ex_vat,
sum(b.gross_profit) as gross_profit,
sum(b.quantity) as basket_quantity_total
from warehouse.d_discounts_new a
inner join warehouse.d_order_products_new b
on a.order_id = b.order_id
where a.discount_description in (select reason_code from majestic_analyst_adh.jca_master_voucher_data_1 where types = 'Reason code')
and b.order_type <> 'Refund'
group by a.discount_description, b.order_id, b.customer_status, b.customer_code, b.first_order, b.order_type, b.store_code,
b.web_order, b.accounting_year, b.accounting_period, b.accounting_week, b.transaction_date
union all
select
a.voucher_code as voucher_barcode,
'unique code' as voucher_description,
b.order_id,
a.voucher_amount as voucher_value,
b.customer_status,
b.customer_code,
b.first_order,
b.order_type,
b.store_code,
b.web_order,
b.accounting_year,
b.accounting_period,
b.accounting_week,
b.transaction_date as order_date,
sum(b.sale_price_ex_vat) as sale_price_ex_vat,
sum(b.gross_profit) as gross_profit,
sum(b.quantity) as basket_quantity_total
from
warehouse.d_orders_new a
INNER JOIN
warehouse.d_order_products_new b
ON
a.order_id = b.order_id
where
isnull(unique_voucher_code_ind, 'N') = 'Y'
AND
a.voucher_code in (select online_code from majestic_analyst_adh.jca_master_voucher_data_1 where types = 'Unique Code')
and
b.order_type <> 'Refund'
group by
a.voucher_code,
b.order_id,
a.voucher_amount,
b.customer_status,
b.customer_code,
b.first_order,
b.order_type,
b.store_code,
b.web_order,
b.accounting_year,
b.accounting_period,
b.accounting_week,
b.transaction_date
;

---- 3.) HAVE SOME DUPLICATES WHERE THE ORDER ID HAS A REASON CODE AND A VOUCHER CODE
----     SO GET RID OF THE ONE WITH THE REASON CODE (WHICH HAS THE WRONG ORDER METRICS)

drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_3;

create table majestic_analyst_adh.jca_all_vouchers_redeemed_3 as
select voucher_barcode, voucher_description, order_id, redeemed_value as redeemed_value_inc_vat,
customer_status, customer_code, first_order, order_type,
store_code, web_order,
accounting_year, accounting_period, accounting_week, order_date,
sale_price_ex_vat, gross_profit, basket_quantity_total
from
(select voucher_barcode, voucher_description, order_id, redeemed_value,
customer_status, customer_code, first_order, order_type,
store_code, web_order,
accounting_year, accounting_period, accounting_week, order_date,
sale_price_ex_vat, gross_profit, basket_quantity_total,
row_number() over(partition by order_id order by
voucher_barcode asc) as rn
from majestic_analyst_adh.jca_all_vouchers_redeemed_2a)
where rn = 1;


---- 3b.) For Voucher Barcodes only.
-- PUT TODAY'S DATE AS END DATE FOR VOUCHERS WITH NO END DATE
-- Tidy up the formats of input fields from the master voucher tracker
-- Fields with blank, non null values will cause issues later on

drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_3b;

create table majestic_analyst_adh.jca_all_vouchers_redeemed_3b as
select unique_id, owner, team, types as type, objective, mechanic, detail,
channel, partner, campaign_name,
to_date(start_date,'DD/MM/YYYY') as start_date,       --- JCA --- this needs fixing either in upload or earlier on in the script
case when expiry_date is null then null else to_date(expiry_date,'DD/MM/YYYY') end as expiry_date_2, --- this needs fixing either in upload or earlier on in the script
expiry_date as expiry_date,
payment_type, online_code, voucher_barcode, reason_code,
case when reach = '' then null else reach end as reach, -- remove non null blanks
case when third_party_funding = '' then null else third_party_funding end as third_party_funding,
case when unit_cost_to_partner = '' then null else unit_cost_to_partner end as unit_cost_to_partner,
case when cpa = '' then null else cpa end as cpa,
case when total_media_spend = '' then null else total_media_spend end as total_media_cost
from majestic_analyst_adh.jca_master_voucher_data_1
;



---- 3c.) Reason Codes
-- JOIN WITH MASTER VOUCHER DATA TABLE TO CLASSIFY WHERE WE CAN - FOR REASON CODES

drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_3c;

create table majestic_analyst_adh.jca_all_vouchers_redeemed_3c as
select a.voucher_barcode, a.voucher_description, a.order_id, a.redeemed_value_inc_vat,
a.customer_status, a.customer_code, a.first_order, a.order_type,
a.store_code, a.web_order,
a.accounting_year, a.accounting_period, a.accounting_week, a.order_date,
a.sale_price_ex_vat, a.gross_profit, a.basket_quantity_total,
vd.unique_id, vd.owner, vd.team, vd.type, vd.objective,
vd.mechanic, vd.detail, vd.channel,
vd.partner, vd.campaign_name,
vd.start_date as campaign_start_date,
vd.expiry_date_2,
vd.payment_type,
vd.reach, vd.third_party_funding, vd.unit_cost_to_partner, vd.cpa, vd.total_media_cost
from majestic_analyst_adh.jca_all_vouchers_redeemed_3 a
left join majestic_analyst_adh.jca_all_vouchers_redeemed_3b vd
on a.voucher_barcode = vd.reason_code
and a.order_date >= vd.start_date
and a.order_date <= vd.expiry_date_2
where lower(vd.type) = 'reason code'
;


---- 4.) Store Voucher Codes
-- JOIN WITH MASTER VOUCHER DATA TABLE TO CLASSIFY WHERE WE CAN - FOR STORE VOUCHER CODES
----

drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_4;

create table majestic_analyst_adh.jca_all_vouchers_redeemed_4 as
select a.voucher_barcode, a.voucher_description, a.order_id, a.redeemed_value_inc_vat,
a.customer_status, a.customer_code, a.first_order, a.order_type,
a.store_code, a.web_order,
a.accounting_year, a.accounting_period, a.accounting_week, a.order_date,
a.sale_price_ex_vat, a.gross_profit, a.basket_quantity_total,
vd.unique_id, vd.owner, vd.team, vd.type, vd.objective,
vd.mechanic, vd.detail, vd.channel,
vd.partner, vd.campaign_name, vd.start_date as campaign_start_date,
vd.expiry_date_2, vd.payment_type,
vd.reach, vd.third_party_funding, vd.unit_cost_to_partner, vd.cpa, vd.total_media_cost
from majestic_analyst_adh.jca_all_vouchers_redeemed_3 a
left join majestic_analyst_adh.jca_all_vouchers_redeemed_3b vd
on trim(a.voucher_barcode)::varchar = trim(vd.voucher_barcode)::varchar
and a.order_date >= vd.start_date
and a.order_date <= vd.expiry_date_2
where lower(vd.type) = 'store voucher'
;


---- 4a.) SAME AGAIN TO GET WEB ORDERS

drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_4a;

create table majestic_analyst_adh.jca_all_vouchers_redeemed_4a as
select a.voucher_barcode, a.voucher_description, a.order_id, a.redeemed_value_inc_vat,
a.customer_status, a.customer_code, a.first_order, a.order_type,
a.store_code, a.web_order,
a.accounting_year, a.accounting_period, a.accounting_week, a.order_date,
a.sale_price_ex_vat, a.gross_profit, a.basket_quantity_total,
vd.unique_id, vd.owner, vd.team, vd.type, vd.objective,
vd.mechanic, vd.detail, vd.channel,
vd.partner, vd.campaign_name, vd.start_date as campaign_start_date,
vd.expiry_date_2, vd.payment_type,
vd.reach, vd.third_party_funding, vd.unit_cost_to_partner, vd.cpa, vd.total_media_cost
from majestic_analyst_adh.jca_all_vouchers_redeemed_3 a
left join majestic_analyst_adh.jca_all_vouchers_redeemed_3b vd
on trim(a.voucher_barcode)::varchar = trim(vd.online_code)::varchar
and a.order_date >= vd.start_date
and a.order_date <= vd.expiry_date_2
where lower(vd.type) = 'online code'
;

---- 4a.) SAME AGAIN TO GET WEB ORDERS

drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_4b;

create table majestic_analyst_adh.jca_all_vouchers_redeemed_4b as
select a.voucher_barcode, a.voucher_description, a.order_id, a.redeemed_value_inc_vat,
a.customer_status, a.customer_code, a.first_order, a.order_type,
a.store_code, a.web_order,
a.accounting_year, a.accounting_period, a.accounting_week, a.order_date,
a.sale_price_ex_vat, a.gross_profit, a.basket_quantity_total,
vd.unique_id, vd.owner, vd.team, vd.type, vd.objective,
vd.mechanic, vd.detail, vd.channel,
vd.partner, vd.campaign_name, vd.start_date as campaign_start_date,
vd.expiry_date_2, vd.payment_type,
vd.reach, vd.third_party_funding, vd.unit_cost_to_partner, vd.cpa, vd.total_media_cost
from majestic_analyst_adh.jca_all_vouchers_redeemed_3 a
left join majestic_analyst_adh.jca_all_vouchers_redeemed_3b vd
on trim(a.voucher_barcode)::varchar = trim(vd.online_code)::varchar --AA
and a.order_date >= vd.start_date
and a.order_date <= vd.expiry_date_2
where lower(vd.type) = 'unique code'
;



---- 4b.) UNION ALL TOGETHER -- FINAL STEP

drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed;

create table majestic_analyst_adh.jca_all_vouchers_redeemed as
select voucher_barcode, voucher_description, order_id, redeemed_value_inc_vat,
customer_status, customer_code, first_order, order_type,
store_code, web_order,
accounting_year, accounting_period, accounting_week, order_date,
sale_price_ex_vat, gross_profit, basket_quantity_total,
unique_id, owner, team, type, objective,
mechanic, detail, channel,
partner, campaign_name, campaign_start_date,
expiry_date_2 as expiry_date, payment_type,
reach, third_party_funding, unit_cost_to_partner, cpa, total_media_cost
from majestic_analyst_adh.jca_all_vouchers_redeemed_3c
union all
select voucher_barcode, voucher_description, order_id, redeemed_value_inc_vat,
customer_status, customer_code, first_order, order_type,
store_code, web_order,
accounting_year, accounting_period, accounting_week, order_date,
sale_price_ex_vat, gross_profit, basket_quantity_total,
unique_id, owner, team, type, objective,
mechanic, detail, channel,
partner, campaign_name, campaign_start_date,
expiry_date_2 as expiry_date, payment_type,
reach, third_party_funding, unit_cost_to_partner, cpa, total_media_cost
from majestic_analyst_adh.jca_all_vouchers_redeemed_4
union all
select voucher_barcode, voucher_description, order_id, redeemed_value_inc_vat,
customer_status, customer_code, first_order, order_type,
store_code, web_order,
accounting_year, accounting_period, accounting_week, order_date,
sale_price_ex_vat, gross_profit, basket_quantity_total,
unique_id, owner, team, type, objective,
mechanic, detail, channel,
partner, campaign_name, campaign_start_date,
expiry_date_2 as expiry_date, payment_type,
reach, third_party_funding, unit_cost_to_partner, cpa, total_media_cost
from majestic_analyst_adh.jca_all_vouchers_redeemed_4a
union all
select voucher_barcode, voucher_description, order_id, redeemed_value_inc_vat,
customer_status, customer_code, first_order, order_type,
store_code, web_order,
accounting_year, accounting_period, accounting_week, order_date,
sale_price_ex_vat, gross_profit, basket_quantity_total,
unique_id, owner, team, type, objective,
mechanic, detail, channel,
partner, campaign_name, campaign_start_date,
expiry_date_2 as expiry_date, payment_type,
reach, third_party_funding, unit_cost_to_partner, cpa, total_media_cost
from majestic_analyst_adh.jca_all_vouchers_redeemed_4b
;


------ ****************************************************************
---- ************   VOUCHER TRADING REPORT ************************
------------ *****************************************----------


---- 1.) GET EMAIL STATUS
---- need to fix this!! Once Alex has fixed the email table
drop table if exists majestic_analyst_adh.jca_voucher_trading_report_1;

create table majestic_analyst_adh.jca_voucher_trading_report_1 as
select * from (
	select a.*, b.receive_email_status, b.start_date,
	row_number() over(partition by a.order_id order by
	b.start_date asc) as rn
from majestic_analyst_adh.jca_all_vouchers_redeemed a
left outer join warehouse.d_customers_history_new b
on a.customer_code = b.customer_code
and a.order_date between b.start_date -7 and b.start_date)
where rn = 1
;



---- 2.) CREATE CUSTOMER STATUS 'VOUCHER ABUSE' FOR DOUBLE REDEMPTIONS

drop table if exists majestic_analyst_adh.jca_voucher_trading_report_2;

create table majestic_analyst_adh.jca_voucher_trading_report_2 as
select voucher_barcode, voucher_description, order_id,
case when customer_row = 1 then customer_status else 'Voucher Abuse' end as customer_status,
customer_code, first_order, order_type, store_code, web_order, accounting_year, accounting_period,
accounting_week, order_date, sale_price_ex_vat as sales_ex_vat, gross_profit as gp, basket_quantity_total, team, type,
objective, mechanic, campaign_start_date, channel, partner, campaign_name,
unique_id, receive_email_status, start_date,
reach, third_party_funding, unit_cost_to_partner, cpa, total_media_cost, redeemed_value_inc_vat
from (
select *, row_number() over(partition by voucher_barcode, customer_code order by
order_date asc) as customer_row
from majestic_analyst_adh.jca_voucher_trading_report_1)
;


---- 3.) UNION BACK ON TYPE ZEROS

drop table if exists majestic_analyst_adh.jca_voucher_trading_report_3;

create table majestic_analyst_adh.jca_voucher_trading_report_3 as
select voucher_barcode, voucher_description, order_id, customer_status,
customer_code, first_order, order_type, store_code, web_order,
accounting_year, accounting_period, accounting_week, order_date,
sales_ex_vat, gp, basket_quantity_total,
team, type, objective, mechanic, campaign_start_date, channel, partner, campaign_name,
unique_id, receive_email_status,
reach::numeric as reach, third_party_funding::numeric as third_party_funding, unit_cost_to_partner::numeric(6,3) as unit_cost_to_partner, cpa::numeric as cpa, total_media_cost::numeric as total_media_cost, redeemed_value_inc_vat
from majestic_analyst_adh.jca_voucher_trading_report_2
union all
select voucher_barcode, voucher_description, order_id, customer_status,
customer_code, first_order, order_type, store_code, web_order,
accounting_year, accounting_period, accounting_week, order_date,
sale_price_ex_vat  as sales_ex_vat, gross_profit as gp, basket_quantity_total,
team, type, objective, mechanic, campaign_start_date, channel, partner, campaign_name,
unique_id,
null as receive_email_status,
reach::numeric as reach, third_party_funding::numeric as third_party_funding, unit_cost_to_partner::numeric(6,3) as unit_cost_to_partner, cpa::numeric as cpa, total_media_cost::numeric as total_media_cost, redeemed_value_inc_vat
----, null as start_date
from majestic_analyst_adh.jca_all_vouchers_redeemed
where customer_status = 'Unknown'
;

-- Add in the LATEST customer quality score for new customers

drop table if exists majestic_analyst_adh.jca_voucher_trading_report_3a;

create table majestic_analyst_adh.jca_voucher_trading_report_3a as
select a.*, b.customer_value_segment
from majestic_analyst_adh.jca_voucher_trading_report_3 a
left join (select customer_code, customer_value_segment
		  from warehouse.d_new_segmentation_status_history_new
		  where accounting_week_start_date between current_date - 13 and current_date - 7
		  ) b
on a.customer_code = b.customer_code
;


---- Don't have right number of rows right now and not sure why.... it's not far off
----
---- 4.) AGGREGATE

drop table if exists majestic_analyst_adh.jca_voucher_trading_report_4;

create table majestic_analyst_adh.jca_voucher_trading_report_4 as
select a.team, a.type, a.objective, a.mechanic, a.channel,
reach, third_party_funding, unit_cost_to_partner, cpa, total_media_cost,
case when a.web_order = 'Y' then 'Web' else 'Store' end as order_channel,
a.partner, a.campaign_name, a.voucher_barcode, a.voucher_description,
extract (year from a.accounting_year) as year,
a.accounting_period as period,
a.accounting_week as week,
count(distinct a.customer_code) as total_customers,
sum(redeemed_value_inc_vat) as total_voucher_redemption_value,
count (distinct (case when a.customer_status = 'New' and a.first_order = 'Y' then a.customer_code else null end)) as new_customers,
count (distinct (case when a.customer_status = 'New' and a.first_order = 'N' then a.customer_code else null end)) as reactivated_customers,
count (distinct (case when a.customer_status = 'Nurture' then a.customer_code else null end)) as nurture_customers,
count (distinct (case when a.customer_status = 'Unknown' then a.customer_code else null end)) as type_zero_customers,
count (distinct (case when a.customer_status in ('Repeat', 'Loyal') then a.customer_code else null end)) as mature_customers,
count (distinct (case when a.customer_status in ('Voucher Abuse') then a.order_id else null end)) as voucher_abuse,
count (distinct a.order_id) as orders,
count (distinct (case when a.basket_quantity_total > 1 then a.order_id else null end)) as upsells,
count (distinct (case when a.receive_email_status = 'email accepted' then a.customer_code else null end)) as all_emails_collected,
count (distinct (case when a.receive_email_status = 'email accepted' and a.customer_status = 'New' then a.customer_code else null end)) as new_emails_collected,
sum (a.gp) as gross_profit,
sum (a.sales_ex_vat)*1.2 as sale_price_inc_vat,
sum (a.sales_ex_vat) as sale_price_ex_vat,
sum (a.basket_quantity_total) as quantity,
sum(case when a.customer_status = 'New' and a.customer_value_segment = 'High' then 1 else null end) as high_value_new
from majestic_analyst_adh.jca_voucher_trading_report_3a a
group by a.team, a.type, a.objective, a.mechanic, a.channel,
reach, third_party_funding, unit_cost_to_partner, cpa, total_media_cost,
a.web_order,
a.partner, a.campaign_name, a.voucher_barcode, a.voucher_description,
a.accounting_year, a.accounting_period, a.accounting_week
;


-- Add in flag for latest week and period for use as filters in Tableau
-- Classify campaigns using the first letters of campaign names into different campaign types

drop table if exists majestic_analyst_adh.jca_voucher_trading_report;

create table majestic_analyst_adh.jca_voucher_trading_report as
select a.*,
b.week_in_year as current_week, b.period as current_period,
b.week_in_period,
case when upper(a.campaign_name) similar to '(PDL|PDS|MCA|MS|DOC|DIG|ISR|ISD)%' then
left(upper(campaign_name), position(' ' in campaign_name) -1) else null end as campaign_type
from majestic_analyst_adh.jca_voucher_trading_report_4 a
left join tableau.current_accounting_dates b
on a.year = b.current_year
and a.week = b.week_in_year
;

grant select on majestic_analyst_adh.jca_master_voucher_data_1 to group "business_user";

grant select on majestic_analyst_adh.jca_all_vouchers_redeemed to group business_user;
grant select on majestic_analyst_adh.jca_all_vouchers_redeemed_1 to group "business_user";
grant select on majestic_analyst_adh.jca_all_vouchers_redeemed_2 to group "business_user";
grant select on majestic_analyst_adh.jca_all_vouchers_redeemed_2a to group "business_user";

grant select on majestic_analyst_adh.jca_voucher_trading_report to group business_user;

----
drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_3;
drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_3a;
drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_3b;
drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_3c;
drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_4;
drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_4a;
drop table if exists majestic_analyst_adh.jca_all_vouchers_redeemed_4b;
drop table if exists majestic_analyst_adh.jca_voucher_trading_report_1;
drop table if exists majestic_analyst_adh.jca_voucher_trading_report_2;
drop table if exists majestic_analyst_adh.jca_voucher_trading_report_3;
drop table if exists majestic_analyst_adh.jca_voucher_trading_report_3a;
drop table if exists majestic_analyst_adh.jca_voucher_trading_report_4;



-- ******************* END OF VOUCHER TRADING REPORT **********************
-- ******************* END OF VOUCHER TRADING REPORT  **********************
