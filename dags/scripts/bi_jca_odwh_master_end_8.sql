-- *************************************    START OF SCRIPT END CHECK   *******************************************************
-- ***** CHECK what time the daily script ENDS ***********************************************************

drop table if exists majestic_analyst_adh.jca_remote_script_test_2;

create table majestic_analyst_adh.jca_remote_script_test_2 as
select current_timestamp as script_ended
from warehouse.d_orders
; 


-- *************************************    END   *******************************************************
