
-- ******************* START OF PROMPTED ORDER CLASSIFICATION REPORT  **********************
-- ******************* START OF PROMPTED ORDER CLASSIFICATION REPORT  **********************


-- Create a series of buckets to define whether a customer, or specifically order can be classified as "prompted", "influenced" or "unprompted"
-- For trade meeting, starting 30th October 2018.

-- Determine in order of "Confidence"
-- Data for 12 week rolling period

drop table if exists majestic_analyst_adh.jca_trade_prompted_order_classification_1;


create table majestic_analyst_adh.jca_trade_prompted_order_classification_1 as
select a.order_id, a.order_date, a.accounting_year, a.accounting_period, a.accounting_week, a.customer_status, a.web_order,
a.customer_code, a.basket_quantity_total, a.sale_price_ex_vat, a.gross_profit, b.transaction_id, c.retail_cust_id,
d.retailcustid, 
case when a.origin = 'WNP' then 'Y' else 'N' end as concierge_order,
case when b.transaction_id is not null then 'Prompted - Web Last Click'
	when a.origin = 'WNP' then 'Prompted - Concierge'  -- Concierge is promted by definition -- does this incldue sign-ups? Question if so
	when e.customer_code is not null or a.voucher_code is not null then 'Prompted - Voucher'
	when c.retail_cust_id is not null then 'Influenced - Email'
	when d.retailcustid is not null then 'Influenced - Direct Mail'
else 'Unprompted' end as order_influence_group_detail,
case when b.transaction_id is not null or a.origin = 'WNP'
		or e.customer_code is not null or a.voucher_code is not null then 'Activated/Prompted'  -- Concierge is promted by definition -- does this incldue sign-ups? Question if so then 'Prompted'
	when c.retail_cust_id is not null or d.retailcustid is not null then 'Influenced'
else 'Unprompted' end as order_influence_group,
f.accept_mailings, f.accept_emails

from warehouse.d_orders_new a
left join (select transaction_id -- Any last-click order placed online is prompted
	from majestic_analyst_adh.jca_ga_webtransactions_upload
	where channel_grouping in ('Paid Search (unknown)','Display & Other Advertising','Affiliates','Social','Email','Branded Paid Search','Generic Paid Search','Paid Search')
	group by transaction_id
) b
on coalesce(a.lego_order_id, a.web_order_no) = b.transaction_id --add in email opens data
left join (select retail_cust_id, b.year as accounting_year, b.week_in_year as accounting_week -- Assume that any email opened in a financial week means the sale was influenced
	from majestic.warehouse.majestic_adestra_export a
	inner join warehouse.l_mjw_accounting_dates b
	on a.timestamp between b.start_date and b.end_date
	where event_name = 'read'
	group by retail_cust_id, b.year, b.week_in_year 
) c
on a.customer_code = c.retail_cust_id
and a.accounting_year = c.accounting_year
and a.accounting_week = c.accounting_week
left join (select retailcustid, b.start_date, b.year, b.week_in_year, b.week_in_month -- Find all weeks where a customer transaction counts as influenced - i.e has received a DM in the 28 days prior
	from majestic_analyst_adh.jca_dm_report_1 a
	join warehouse.l_mjw_accounting_dates b
	on b.start_date between a.mailing_period_start  and a.mailing_period_start + 28 -- Any mailing in last 28 days counts as influenced
	where mailing_period_start > current_date - (1100) -- Limit query by date
	group by retailcustid, b.start_date, b.year, b.week_in_year, b.week_in_month
) d
on a.customer_code = d.retailcustid
and a.accounting_year = d.year
and a.accounting_week = d.week_in_year
left join (select order_id, customer_code
	from warehouse.d_vouchers_redeemed_new  -- If a voucher was redeemed then transaction was prompted
	group by order_id, customer_code
	) e
	on a.order_id = e.order_id

left join warehouse.d_customers_history_new f -- Add in customer contactability by DM and Email at the time of the transaction
on a.customer_code = f.customer_code
and a.order_date between f.start_date and f.end_date
	where a.customer_type = 'Retail'
	and a.store_code not in ('701','702','703','704')
	and order_date > (select min(start_date) 
					from warehouse.l_mjw_accounting_dates 
					where start_date > current_date - (62 * 12))
;



drop table if exists majestic_analyst_adh.jca_trade_prompted_order_classification_2;

create table majestic_analyst_adh.jca_trade_prompted_order_classification_2 as
select c.*, datediff(day,last_mailed_date,order_date::timestamp) as days_since_mailed_Date,
case when last_mailed_date is null then 'Never Mailed'
when datediff(day,last_mailed_date,order_date::timestamp)  <= 180 then '<6M'
when datediff(day,last_mailed_date,order_date::timestamp)  <= 365 then '6-12M'
else '>12M' end as months_since_last_mailed
from (
	select a.*, max(b.mailing_period_start) as last_mailed_date
	from majestic_analyst_adh.jca_trade_prompted_order_classification_1 a
	left join majestic_analyst_adh.jca_dm_report_1 b
	on a.customer_code = b.retailcustid
	and b.mailing_period_start <= a.order_date
	group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
	) c
;

--- Aggregate by ACCOUNTING PERIOD:

drop table if exists majestic_analyst_adh.jca_trade_prompted_order_classification_3;

create table majestic_analyst_adh.jca_trade_prompted_order_classification_3 as

select a.*, b.start_date as week_start_date
from (
select accounting_year, accounting_week, customer_status, 
case when customer_status in ('New', 'Nurture') then 'New' else 'Repeat' end as customer_status_group,
concierge_order,
order_influence_group, order_influence_group_detail,
months_since_last_mailed,
case when accept_mailings is null and accept_emails is null then 'NOT CONTACTABLE'
	when accept_mailings = 'Y' and accept_emails = 'Y' then 'DM & EM'
	when accept_mailings = 'N' and accept_emails = 'Y' then 'EM'
	when accept_mailings = 'Y' and accept_emails = 'N' then 'DM'
else 'UNKNOWN' end as marketing_contactability,	
count(*) as records, count(distinct order_id) as orders, sum(sale_price_ex_vat) as sales_ex_vat, sum(gross_profit) as gross_profit,
web_order, accounting_period
from majestic_analyst_adh.jca_trade_prompted_order_classification_2
group by accounting_year, accounting_week, customer_status, web_order, accounting_period,
case when customer_status in ('New', 'Nurture') then 'New' else 'Repeat' end,
concierge_order,
order_influence_group, order_influence_group_detail,
months_since_last_mailed,
case when accept_mailings is null and accept_emails is null then 'NOT CONTACTABLE'
	when accept_mailings = 'Y' and accept_emails = 'Y' then 'DM & EM'
	when accept_mailings = 'N' and accept_emails = 'Y' then 'EM'
	when accept_mailings = 'Y' and accept_emails = 'N' then 'DM'
else 'UNKNOWN' end
) a
inner join warehouse.l_mjw_accounting_dates b
on a.accounting_year = b.year
and a.accounting_week = b.week_in_year
;



--Aggregate by mailed, not amiled
-- This is currently showing by accounting week

drop table if exists majestic_analyst_adh.jca_trade_prompted_order_classification_4;

create table majestic_analyst_adh.jca_trade_prompted_order_classification_4 as
select accounting_year, accounting_period, accounting_week,
case when months_since_last_mailed = 'Never Mailed' then 'Never Mailed' else 'Mailed' end as direct_mail_group,
count(distinct customer_code) as customers,
count(*) as records, count(distinct order_id) as orders, sum(sale_price_ex_vat) as sales_ex_vat, sum(gross_profit) as gross_profit
from majestic_analyst_adh.jca_trade_prompted_order_classification_2
where concierge_order = 'N'
and extract('year' from accounting_year) in ('2019','2020')
and accounting_week between 1 and (select week_in_year from tableau.current_accounting_dates)
and web_order = 'N'
group by accounting_year, accounting_period, accounting_week,
case when months_since_last_mailed = 'Never Mailed' then 'Never Mailed' else 'Mailed' end
;


drop table majestic_analyst_adh.jca_trade_prompted_order_classification_1;
drop table majestic_analyst_adh.jca_trade_prompted_order_classification_2;

-- ******************* END OF PROMPTED ORDER CLASSIFICATION REPORT  **********************
-- ******************* END OF PROMPTED ORDER CLASSIFICATION REPORT  **********************
