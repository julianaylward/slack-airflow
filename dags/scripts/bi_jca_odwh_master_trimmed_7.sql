-- ******************* START OF GROW MY STORE TRIMMED METRICS REPORT  **********************
-- ******************* START OF GROW MY STORE TRIMMED METRICS REPORT **********************


drop table if exists majestic_analyst_adh.d_refunds_matched_to_sales;
Create table majestic_analyst_adh.d_refunds_matched_to_sales as(
with all_orders_and_refunds as(
select
o.customer_code,
o.order_date_with_time as order_date,

case when web_order = 'Y' then 'In Store' else 'Web' end as origin,

case when o.concierge in ('Concierge Signup 3 Free', 'Concierge Signup Other') then 'Concierge Signup'
when o.concierge = 'Concierge Member' then 'Concierge Member'
when o.scheme_event_name in ('Wine Plus', 'Lock It In') then 'Lock it in' -- note case sensitive - required for join with targets
else 'Normal' end as concierge,

case when customer_type in ('Commercial', 'Staff') then customer_type else customer_status
end as customer_group,

store_code,
sum(o.list_price) as value,
sum(o.sale_price_ex_vat) as value2
from warehouse.d_orders o
where order_date > getdate() - (365*3)
group by 1,2,3,4,5,6
)
,all_orders as(
select 
o.customer_code,
o.order_date,
store_code,
value,
value2
from
all_orders_and_refunds o
where 
value2 > 0
and origin = 'In Store'
and concierge = 'Normal'
)
,all_refunds as(
select 
o.customer_code,
o.order_date,
store_code,
value*-1 as value,
value2*-1 as value2
from
all_orders_and_refunds o
where 
value2 < 0
and origin = 'In Store'
and concierge = 'Normal'
)
--Join table to match all orders on days from customers to all refunds duplicates all customer orders for every refund. 
--remove records refunded before order and there the refund amount was larger than order. and remove type Zeros
--using 2 diffrent value definers as sometimes they can differ
,join_orders_and_refunds as(
select
o.customer_code,
o.store_code,
o.order_date as order_date,
r.order_date as refund_date,
r.order_date - o.order_date as days_since_order
from
all_orders o
left join all_refunds r
on o.customer_code = r.customer_code
and o.store_code = r.store_code
where r.order_date > o.order_date
and (o.value >= r.value or o.value2 >= r.value2)
and o.customer_code > 0
)
--rank the difference between the order date and refund date to find most viable matching order day for each refund
,ranking_days_since_order as(
select
customer_code,
store_code,
refund_date,
order_date,
row_number() over(partition by customer_code,refund_date order by days_since_order asc) as Rank
from join_orders_and_refunds
)
--,master_table as(
select 
a.customer_code,
a.store_code,

origin,
concierge,
customer_group,

a.order_date::date,
b.refund_date::date
from all_orders_and_refunds a
left join
(select * from ranking_days_since_order where rank = 1) b
on a.origin = 'In Store'
and a.concierge = 'Normal'
and a.customer_code = b.customer_code
and a.store_code = b.store_code


where 
value2 > 0

)
;

drop table if exists majestic_analyst_adh.d_consolidated_orders_for_aov;
Create table majestic_analyst_adh.d_consolidated_orders_for_aov as(
with all_orders1 as(
select 
customer_code,
store_code,
accounting_year,
accounting_period,
accounting_week,
transaction_date,

case when web_order = 'Y' then 'Web' else 'In Store' end as origin,

case when op.concierge in ('Concierge Signup 3 Free', 'Concierge Signup Other') then 'Concierge Signup'
when op.concierge = 'Concierge Member' then 'Concierge Member'
when o.scheme_event_name in ('Wine Plus', 'Lock It In') then 'Lock it in' -- note case sensitive - required for join with targets
else 'Normal' end as concierge,

case when customer_type in ('Commercial', 'Staff') then customer_type else customer_status
end as customer_group,

case when sale_price_inc_vat < 0 then 'Refund' else 'Sale' end as order_type,

quantity,
sale_price_inc_vat,
sale_price_ex_vat,
gross_profit,
case when product_group not in ('33','36','26','27','28','29','41','94') then quantity end as total_bottles
from 
warehouse.d_order_products op
left join
(select order_id, origin,scheme_event_name from warehouse.d_orders where order_date > getdate() - (365*3)) o
using (order_id)
where 
product_code not in ('33003','33010','33036','33037','36279','36410','36050','36100','36200')
and transaction_date > getdate() - (365*3)
and op.order_id not in ('1401295121','1401295123','1401295124','1401295125')
)

,all_orders_consolidated as(
select
customer_code,
store_code,
accounting_year,
accounting_period,
accounting_week,
transaction_date,

origin,
concierge,
customer_group,

order_type,

sum(quantity) as quantity,
sum(sale_price_inc_vat) as sale_price_inc_vat,
sum(sale_price_ex_vat) as sale_price_ex_vat,
sum(gross_profit) as gross_profit,
sum(total_bottles) as total_bottles

from
all_orders1

group by 1,2,3,4,5,6,7,8,9,10

)


--,all_transactions as(
select
a.accounting_year,
accounting_period,
a.accounting_week,
transaction_date,

customer_code,
store_code,

origin,
concierge,
customer_group,

order_type,

sum(quantity) as quantity,
sum(sale_price_inc_vat) as sale_price_inc_vat,
sum(sale_price_ex_vat) as sale_price_ex_vat,
sum(gross_profit) as gross_profit,
sum(total_bottles) as total_bottles
from all_orders_consolidated a
group by 1,2,3,4,5,6,7,8,9,10
--)

)

;

drop table if exists majestic_analyst_adh.d_fully_consolidated_orders;
Create table majestic_analyst_adh.d_fully_consolidated_orders as(

select
o.customer_code,
o.transaction_date,
o.accounting_week,
o.accounting_period,
extract(year from o.accounting_year) as accounting_year,
dates.comparison_week_in_year,
extract(year from dates.comparison_year) as comparison_year,

case when o.store_code = '030' then '268' else o.store_code end as store_code,

o.origin,
o.concierge,
o.customer_group,



sum(o.sale_price_ex_vat) - NVL(sum(r.sale_price_ex_vat),0) as total_sales_price_ex_vat,
sum(o.sale_price_inc_vat) - NVL(sum(r.sale_price_inc_vat),0) as total_sales_price_inc_vat,
sum(o.gross_profit) - NVL(sum(r.gross_profit),0) as total_gp,
sum(o.total_bottles) - NVL(sum(r.total_bottles),0) as total_bottles,
count(distinct o.customer_code || '_' || o.transaction_date) as total_transactions
from (select * from majestic_analyst_adh.d_consolidated_orders_for_aov where sale_price_inc_vat> 0) o
Left join majestic_analyst_adh.d_refunds_matched_to_sales m
on o.customer_code = m.customer_code
and o.transaction_date::date = m.order_date::date
and o.store_code = m.store_code
and o.origin = m.origin
and o.concierge = m.concierge
and o.customer_group = m.customer_group
and o.order_type = 'Sale' 
and o.origin = 'In Store' 
left join (select * from majestic_analyst_adh.d_consolidated_orders_for_aov where order_type = 'Refund' and origin = 'In Store' and concierge = 'Normal') r
on m.customer_code = r.customer_code
and m.refund_date = r.transaction_date
and m.store_code = r.store_code
and m.origin = r.origin
and m.concierge = r.concierge
and m.order_date = m.refund_date
left join warehouse.l_mjw_accounting_dates dates
on o.transaction_date between dates.start_date and dates.end_date
left join warehouse.l_mjw_accounting_dates rd
on m.refund_date between rd.start_date and rd.end_date
left join warehouse.d_stores s
on o.store_code = s.store_code
group by 1,2,3,4,5,6,7,8,9,10,11
--)
);



grant all on majestic_analyst_adh.d_refunds_matched_to_sales to group "business_user";
grant all on majestic_analyst_adh.d_consolidated_orders_for_aov to group "business_user";
grant all on majestic_analyst_adh.d_fully_consolidated_orders to group "business_user";

-- ******************* END OF GROW MY STORE TRIMMED METRICS REPORT  **********************
-- ******************* END OF GROW MY STORE TRIMMED METRICS REPORT **********************

