-- ******************* START OF RETENTION REPORT **********************
-- ******************* START OF RETENTION REPORT **********************


-- Find every customer who has lapsed
--Repeat and Loyal customers only - we don't care about Nurture attrition as such
-- Add in calendar dates
-- Exclude commercial, calais and staff to align with customer team targets
-- Add in customer value segment

drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_1_scv;

create table majestic_analyst_adh.jca_lapsed_report_simple_1_scv as
select a.week_start_date, a.week_end_date, 
a.previous_status, a.customer_status, a.customer_code,
b.new_customer_quality_score,
b.new_customer_quality_score_decile,
b.lapsed_cust_quality_score,
b.lapsed_cust_quality_score_decile,
b.customer_value_segment,
b.customer_status_new,
c.year,
c.period,
c.week_in_year,
c.week_in_month
from warehouse.r_customer_status_movement_by_week_new a
left join warehouse.d_new_segmentation_status_history_new b
on a.customer_code = b.customer_code
and a.week_start_date = b.accounting_week_start_date
inner join warehouse.l_mjw_accounting_dates c
on a.week_start_date = c.start_date
where a.customer_type = 'Retail'
and left(lpad(a.customer_code,9,0),3) not in ('701','702','703','704')
and a.customer_status = 'Lapsed'
and a.previous_status is not null
and a.previous_status not in ('Lapsed','Nurture')
and c.year >= 2017
;


-- Summarise by week, customer_status (this should only be Repeat)
-- And by customer value segment

drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_2_scv;

create table majestic_analyst_adh.jca_lapsed_report_simple_2_scv as
select year, period, week_in_year, week_in_month,
case when customer_value_segment = 'High' then 'High' else 'Low' end as customer_value_segment,
count(distinct customer_code) as lapsing_customers
from majestic_analyst_adh.jca_lapsed_report_simple_1_scv 
group by year, period, week_in_year, week_in_month,
case when customer_value_segment = 'High' then 'High' else 'Low' end
;


-- join in the size of the customer base
-- For the Week
-- At Start of period
-- At start of FY
-- For Repeat and Loyal customers
-- This is required as retention is measured against the average size of the customer base over the period of interest

drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_3_scv;

create table majestic_analyst_adh.jca_lapsed_report_simple_3_scv as
select year, week_in_year, period, week_in_month, customer_value_segment,
count(distinct customer_code) as customers, 
count(*) as records
from (
select a.accounting_week_start_date, b.year, b.period, b.week_in_year, b.week_in_month,
a.customer_code,
a.customer_status,
case when c.customer_value_segment = 'High' then 'High' else 'Low' end as customer_value_segment
from warehouse.d_customer_status_history_by_week_new a
inner join warehouse.l_mjw_accounting_dates b
on a.accounting_week_start_date = b.start_date
left join warehouse.d_new_segmentation_status_history_new c
on a.customer_code = c.customer_code
and a.accounting_week_start_date = c.accounting_week_start_date
where a.customer_type = 'Retail'
and left(lpad(a.customer_code,9,0),3) not in ('701','702','703','704')
and a.customer_status in ('Repeat', 'Loyal')
and b.year >= 2017
) 
group by year, week_in_year, period, week_in_month,
customer_value_segment
;


-- Join the active base data with Lapsing data
-- Group up R&L base
-- Assume Unknown customer value segment customers are Low Value

drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_4_scv;

create table majestic_analyst_adh.jca_lapsed_report_simple_4_scv as
select a.year, a.period, a.week_in_year, a.week_in_month,
a.customer_value_segment,
sum(a.lapsing_customers) as lapsing_customers,
sum(b.customers) as active_customer_base_week
from majestic_analyst_adh.jca_lapsed_report_simple_2_scv a
inner join majestic_analyst_adh.jca_lapsed_report_simple_3_scv b
on a.year = b.year
and a.week_in_year = b.week_in_year
and a.customer_value_segment = b.customer_value_segment
group by a.year, a.period, a.week_in_month,
a.customer_value_segment,  a.week_in_year
;


-- Join in customer base at period start
drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_5_scv;

create table majestic_analyst_adh.jca_lapsed_report_simple_5_scv as
select a.*,
sum(b.customers) as active_customers_base_period_start
from majestic_analyst_adh.jca_lapsed_report_simple_4_scv a
inner join (select *
		from majestic_analyst_adh.jca_lapsed_report_simple_3_scv 
		where week_in_month = 1  -- First week of Period
		) b
on a.year = b.year
and a.period = b.period
and a.customer_value_segment = b.customer_value_segment
group by 1,2,3,4,5,6,7
;


-- join in customer base at FY start
drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_6_scv;

create table majestic_analyst_adh.jca_lapsed_report_simple_6_scv as
select a.*,
sum(b.customers) as active_customers_base_year_start
from majestic_analyst_adh.jca_lapsed_report_simple_5_scv a
inner join (select *
		from majestic_analyst_adh.jca_lapsed_report_simple_3_scv 
		where week_in_year = 1  -- First week of Period
		) b
on a.year = b.year
and a.customer_value_segment  = b.customer_value_segment
group by 1,2,3,4,5,6,7,8
;


-- join in annualisation scores

drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_7_scv;

create table majestic_analyst_adh.jca_lapsed_report_simple_7_scv as
select a.*,
b.weeks_in_month,
c.perc_orders
from majestic_analyst_adh.jca_lapsed_report_simple_6_scv a
inner join (select year, period, max(week_in_month) as weeks_in_month
		from warehouse.l_mjw_accounting_dates 
		group by year, period
		) b
on a.year = b.year 
and a.period = b.period
inner join (select * from warehouse.l_annualisation_adjustment_factors
		where customer_status_group = 'All'
		) c
on a.period = c.period
and a.week_in_year = c.week
;

-- join in annualised retention targets
-- Targets are set at a period level for In-Period and Cumulative YTD

drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_8_scv;

create table majestic_analyst_adh.jca_lapsed_report_simple_8_scv as
select a.*,
b.metric_value as in_period_retention_target,
c.metric_value as ytd_retention_target
from majestic_analyst_adh.jca_lapsed_report_simple_7_scv a
left join (select *
from majestic.majestic_analyst_adh.jca_customer_model_kpis
where budget_type = 'BUDGET_SCV'
and metric_parent = 'RETENTION'
and time_period = 'PERIOD'
and year = extract ('year' from (select year 
								from warehouse.l_mjw_accounting_dates 
								where current_date between start_date and end_date
								)
					) 
) b
on extract('year' from a.year) = b.year 
and a.period = b.period -- above query only returns targets for a single year, so no need to join on year
left join (select *
from majestic.majestic_analyst_adh.jca_customer_model_kpis
where budget_type = 'BUDGET_SCV'
and metric_parent = 'RETENTION'
and time_period = 'YTD'
and year = extract ('year' from (select year 
								from warehouse.l_mjw_accounting_dates 
								where current_date between start_date and end_date
								)
					) 
) c
on extract('year' from a.year) = b.year 
and a.period = c.period -- above query only returns targets for a single year, so no need to join on year
;

drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_1_scv;
drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_2_scv;
drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_3_scv;
drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_4_scv;
drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_5_scv;
drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_6_scv;
drop table if exists majestic_analyst_adh.jca_lapsed_report_simple_7_scv;



-- ******************* END OF RETENTION REPORT **********************
-- ******************* END OF RETENTION REPORT **********************
