-- ***** CHECK what time the daily script starts

drop table if exists majestic_analyst_adh.jca_remote_script_test;

create table majestic_analyst_adh.jca_remote_script_test as
select max(order_date) as d_orders_latest,
current_timestamp as script_started
from warehouse.d_orders
; 