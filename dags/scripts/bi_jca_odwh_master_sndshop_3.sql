-- *********** Start of 2nd Shop Rate Report ****************


-- JA 2nd shop rate / new customer report
-- Created 11/08/2016
-- 31st March = Start of FY 2015

-- Get all first orders (new + reactivated)
-- Retail only
-- Note, over the time period measured, its possible for a customer to join, lapse, reactivate

drop table if exists majestic_analyst_adh.jca_second_shop_rate_1;

create table majestic_analyst_adh.jca_second_shop_rate_1 as
SELECT a.customer_code,  MIN(a.order_date) as first_order_date,
case when a.order_date > b.customer_creation_date then 'reactivated' else 'new' end as customer_substatus
FROM warehouse.d_orders_new a
inner join warehouse.d_customers_new b
on a.customer_code = b.customer_code
-- Ammended 08/11/2016 to allow for variable second shop windows - use Tableau Parameter to control (Second Shop Window)
WHERE a.order_date between current_date - ((365*3)+30) and current_date - (2)  -- max date for first order date allowing cust time to place 2nd shop
-- WHERE order_date between current_date - ((365*3)+30) and current_date - (365 + 30)  -- max date for first order date allowing cust time to place 2nd shop
-- where order_date between current_date - ((365*3)+30) and current_date - (180)
-- AND o.first_order = 'Y'
AND a.customer_status = 'New'
AND a.customer_type = 'Retail'
AND a.basket_quantity_total > 0
and a.sale_price_ex_vat >0
and a.customer_code >= 1
GROUP BY a.customer_code,
case when a.order_date > b.customer_creation_date then 'reactivated' else 'new' end
;

-- Get a (single) order_id for first_orders
-- Arbitrarily take the lesser of any duplicate order_ids as the first, unique order_id
-- Once we have the data, add first_order timestamp as well!!

drop table if exists majestic_analyst_adh.jca_second_shop_rate_1a;

create table majestic_analyst_adh.jca_second_shop_rate_1a as
select a.customer_code, a.first_order_date, a.customer_substatus,
min(b.order_id) as first_order_id
from majestic_analyst_adh.jca_second_shop_rate_1 a
inner join warehouse.d_orders_new b
on a.customer_code = b.customer_code
and a.first_order_date = b.order_date
group by a.customer_code, a.first_order_date, a.customer_substatus
;



-- find customers who have placed an order since then
--Exclude refunds

drop table if exists majestic_analyst_adh.jca_second_shop_rate_2;

create table majestic_analyst_adh.jca_second_shop_rate_2 as
select a.customer_code,
a.customer_substatus,
a.first_order_date,
a.first_order_id,
b.order_date
from majestic_analyst_adh.jca_second_shop_rate_1a a
left join (
select customer_code, order_date
from warehouse.d_orders_new
where basket_quantity_total >= 1
and sale_price_ex_vat >= 1
) b
on a.customer_code = b.customer_code
and b.order_date between a.first_order_date + 1 and  current_date - (7)  --include all shops up to last 10 weeks (allow for variable time windows controlled in Tableau)
--and b.order_date between a.first_order_date + 1 and a.first_order_date + (365 + 30)
-- and b.order_date between a.first_order_date + 1 and a.first_order_date + (180)
;


-- get the min order id and order details for these customers

drop table if exists majestic_analyst_adh.jca_second_shop_rate_3;

create table majestic_analyst_adh.jca_second_shop_rate_3 as
select customer_code, customer_substatus, first_order_date, first_order_id,
next_order_date,
case when next_order_date is not null then 1 else null end as second_shop
from (select customer_code, customer_substatus, first_order_date, first_order_id, min(order_date) as next_order_date
from majestic_analyst_adh.jca_second_shop_rate_2
group by customer_code, customer_substatus, first_order_date, first_order_id
)
;


-- Join in calendar data from accouting_dates

-- Add in number number of bottles in first order, store / online, event_flag, contents of order, has_email_address
-- Add in first store shopped, region,

drop table if exists majestic_analyst_adh.jca_second_shop_rate_4;

create table majestic_analyst_adh.jca_second_shop_rate_4 as
select a.*,
b.month, b.year, b.week_in_month, b.week_in_year, b.period,
c.store_code, c.store_name, c.region_name,
c.first_shop_units, c.first_order_value, c.skus_in_first_order,
c.web_order
from majestic_analyst_adh.jca_second_shop_rate_3 a
inner join warehouse.l_mjw_accounting_dates b
on a.first_order_date between start_date and end_date
inner join (select customer_code, order_id, store_code, web_order,
        store_name, region_name,
        sum(quantity) as first_shop_units,
        sum(sale_price_ex_vat) as first_order_value,
        count(distinct product_code) as skus_in_first_order
        from warehouse.d_order_products_new
        where customer_code in (select distinct customer_code
                                from majestic_analyst_adh.jca_second_shop_rate_1)
        and transaction_date >= (select min(first_order_date) from majestic_analyst_adh.jca_second_shop_rate_1)
        group by customer_code, order_id, store_code, store_name, region_name, web_order
        ) c
on a.customer_code = c.customer_code
and a.first_order_id = c.order_id
;




-- Add in order level data from warehouse.d_orders
-- first order champagne units
-- First order beer units
-- First order wine units

drop table if exists majestic_analyst_adh.jca_second_shop_rate_4a;

create table majestic_analyst_adh.jca_second_shop_rate_4a as
select a.*,
b.event_type,
b.basket_quantity_wine,
b.basket_quantity_sparkling,
b.basket_quantity_spirits,
b.basket_quantity_beer_cider,
b.basket_quantity_total,
b.fulfilled_by_store_code
from majestic_analyst_adh.jca_second_shop_rate_4 a
inner join warehouse.d_orders_new b
on a.first_order_id = b.order_id
;

-- Add in first quarter value.
-- This is another critical measure of new customer quality

drop table if exists majestic_analyst_adh.jca_second_shop_rate_4b;

create table majestic_analyst_adh.jca_second_shop_rate_4b as
select a.customer_code, a.customer_substatus, a.first_order_date, a.first_order_id, a.next_order_date, a.second_shop, a.month, a.year, a.week_in_month, a.week_in_year,
a.period, a.store_code, a.store_name, a.region_name, a.first_shop_units, a.first_order_value, a.skus_in_first_order, a.web_order, a.event_type, 
a.basket_quantity_wine, a.basket_quantity_sparkling, a.basket_quantity_spirits, a.basket_quantity_beer_cider, a.basket_quantity_total, a.fulfilled_by_store_code,
sum(b.sale_price_ex_vat) as first_half_sales,
sum(b.gross_profit) as first_half_gross_profit
from majestic_analyst_adh.jca_second_shop_rate_4a a
inner join warehouse.d_orders_new b
on a.customer_code = b.customer_code
and b.order_date between a.first_order_date and a.first_order_date + 180
group by a.customer_code, a.customer_substatus, a.first_order_date, a.first_order_id, a.next_order_date, a.second_shop, a.month, a.year, a.week_in_month, a.week_in_year,
a.period, a.store_code, a.store_name, a.region_name, a.first_shop_units, a.first_order_value, a.skus_in_first_order, a.web_order, a.event_type, 
a.basket_quantity_wine, a.basket_quantity_sparkling, a.basket_quantity_spirits, a.basket_quantity_beer_cider, a.basket_quantity_total, a.fulfilled_by_store_code
;



-- Add in Welcome Programme Inclusion
-- It should be impossible, at present to reveive the WP twice, and can only therefore return one record per cust below

drop table if exists majestic_analyst_adh.jca_second_shop_rate_4c;

create table majestic_analyst_adh.jca_second_shop_rate_4c as
select a.*, b.campaign_name as welcome_campaign, 
case when c.customer_code is not null then 'Y' else 'N' end as completed_wineify
from majestic_analyst_adh.jca_second_shop_rate_4b a
left join (select retail_cust_id, campaign_name
	from warehouse.majestic_adestra_export
	where campaign_name in ('20180208_W1A','20180208_W1B', '20180208_W1A Store shop thanks', '20180208_W1B Online shop thanks')
	and event_name = 'sent_campaign'
	and retail_cust_id is not null
	) b
on a.customer_code = b.retail_cust_id
left join (select a.email, b.customer_code
	from majestic_analyst_adh.leh_wineify_emails a
	inner join warehouse.d_customers_new b
	on a.email = b.email
	) c
on a.customer_code = c.customer_code
;




-- Add in 5* response data
-- postcodeA, customer has an email address
-- Did they use a voucher?
-- Add in Welcome Model Scores

drop table if exists majestic_analyst_adh.jca_second_shop_rate_4d;

create table majestic_analyst_adh.jca_second_shop_rate_4d as
select a.*,
b.post_code_a,
b.small_business_flag,
b.customer_type,
case when b.email is not null then 'Y' else 'N' end as has_email,
case when c.customer_code is not null then 'Y' else 'N' end as completed_five_star,
c.five_star_rating,
c.did_we_meet_your_expectations,
d.voucher_barcode,
d.voucher_description,
d.voucher_value,
e.new_customer_quality_score,
e.new_customer_quality_score_decile,
f.lapsed_cust_quality_score,
f.lapsed_cust_quality_score_decile
from majestic_analyst_adh.jca_second_shop_rate_4c a
inner join warehouse.d_customers b
on a.customer_code = b.customer_code
left join warehouse.d_five_star_data c
on a.first_order_id = c.order_id
and a.customer_code = c.customer_code       -- this should be overkill, but it seems 5* data is not always correctly attributed
left join (select order_id, voucher_barcode, voucher_description, sum(redeemed_value) as voucher_value
		from warehouse.d_vouchers_redeemed_new 
		group by order_id, voucher_barcode, voucher_description)  d
on a.first_order_id = d.order_id
left join warehouse.d_new_cust_quality_new e
on a.customer_code = e.customer_code
left join warehouse.d_lapsed_cust_quality_new f
on a.customer_code = f.customer_code
;


-- Add in Distance to nearest store
-- NOTE: THIS IS APPROXIMATE ONLY AND NOT AVAILABLE FOR ALL CUSTOMERS!!

drop table if exists majestic_analyst_adh.jca_second_shop_rate_5a;

create table majestic_analyst_adh.jca_second_shop_rate_5a as
select a.*,
b.km_from_store
from majestic_analyst_adh.jca_second_shop_rate_4d a
left join warehouse.d_cust_store_postcode_mapping b
on a.customer_code = b.customer_code
;


-- SWTICH THIS BACK FROM TEMP! AND THEN MOVE TO DAILY SCRIPT TO STOP PERMISSIONS ISSUES

grant select on majestic_analyst_adh.jca_second_shop_rate_5a to group "business_user";
-- Add in first order by first order discount band

-- What voucher was it?
-- What online channel did they come in from
-- Delivery vs. NFC vs. Click & Collect
-- First order contains champagne / NZ SB


-- Drop intermediate tables to save space
drop table if exists majestic_analyst_adh.jca_second_shop_rate_1;
drop table if exists majestic_analyst_adh.jca_second_shop_rate_1a;
drop table if exists majestic_analyst_adh.jca_second_shop_rate_2;
drop table if exists majestic_analyst_adh.jca_second_shop_rate_3;
drop table if exists majestic_analyst_adh.jca_second_shop_rate_4;
drop table if exists majestic_analyst_adh.jca_second_shop_rate_4a;
drop table if exists majestic_analyst_adh.jca_second_shop_rate_4b;
drop table if exists majestic_analyst_adh.jca_second_shop_rate_4c;
drop table if exists majestic_analyst_adh.jca_second_shop_rate_4d;


-- *********** END of 2nd Shop Rate Report ****************
