-- **************************   START OF DM REPORTING SCRIPT   ***************************************************
-- Direct Mail Reporting Script at order level
-- Created by JCA
-- Modified 26/08/2016

-- created by JCA June 2016.
-- Direct Mail Analysis Report for DM Commencing March 2016 to present
-- Report is at order-level, where mailings = parent, order = child
-- uses temp tables (#) where possible to minimate space requirements
-- truncate / insert tables are optimimised for compression settings (analyze compression)

-- merge mailing (contact) history, with mailing test hitory
-- Decode customer_code field

truncate table majestic_analyst_adh.jca_dm_report_1;

insert into majestic_analyst_adh.jca_dm_report_1
SELECT a.*,
case when left(a.customer_code,1) = 'B' then 'Business'
when left(a.customer_code,1) = 'P' then 'UK Private'
when left(a.customer_code,1) = 'S' then 'Scotland'
else 'Unknown'
end as business_private_scot,
case when right(left(a.customer_code,5),1) = 'D' then 'DM Only'
when left(a.customer_code,1) = 'E' then 'Email Only'
when left(a.customer_code,1) = 'M' then 'Multi'
when left(a.customer_code,1) = 'X' then 'Not Contactable'
else 'Unknown'
end as cust_contact_status,
--case when a.creativename like '%|%' then right(a.creativename, len(a.creativename) - position('|' in a.creativename)+ 2) else right(a.creativename,1) end as test_group,
b.mailing_period_start,
b.mailing_period_end,
b.test_group,
b.test_subgroup,
b.is_control,
b.test_description,
b.test_parent_description,
b.incentive,
b.mailing_cost_per_cust,
b.incentive_cogs,
b.incentive_barcode,
b.incentive_barcode_2,
b.incentive_cogs_2,
b.test_group_uid
FROM majestic_analyst_adh.jca_dm_cust_contact_hist_copy a
inner join  (select * from majestic_analyst_adh.jca_dm_test_history_copy
            --where lower(mailing_period) not like '%july%'
) b
on
lower(case when a.creativename like '%|%'
then a.mailing_period || right(a.creativename, len(a.creativename) - position('|' in a.creativename)+ 2)
else a.mailing_period || right(a.creativename,1)
end) =
lower(case when b.test_subgroup is null then b.mailing_period || b.test_group else b.mailing_period || b.test_group ||'|'||b.test_subgroup end)
and a.calendar_year = b.calendar_year
and lower(a.mailing_period) = lower(b.mailing_period)
where a.calendar_year >= 2017
;



create table #jca_dm_report_1a as
select a.*,
b.store_name as store_delivery,
b.region_name as region_delivery
from majestic_analyst_adh.jca_dm_report_1 a
left join warehouse.d_stores b
on a.store_dp_delivery = b.store_code
;

-- join in order details during mailing period

create table #jca_dm_report_2 as
SELECT a.*,
b.order_id,
b.customer_code as customer_id,
b.order_date,
b.customer_type,
b.store_code,
b.store_name,
b.voucher_discount,
b.sale_price_ex_vat,
case when b.sale_price_ex_vat >= 500 then 'Y' else 'N' end as order_is_high_val,   -- flag high value orders - option to limit sales / GP to a max value to reduce volatility
b.order_type,
b.web_order_no,
b.region_name,
b.cogs,
b.gross_profit,
b.first_order,
b.basket_quantity_wine,
b.basket_quantity_sparkling,
b.basket_quantity_spirits,
b.basket_quantity_beer_cider,
b.basket_quantity_other,
b.basket_quantity_total,
b.event_type
FROM #jca_dm_report_1a a
left join warehouse.d_orders b
on a.retailcustid = b.customer_code
and b.order_date between
a.mailing_period_start and case when a.mailing_period_end < current_date
    then a.mailing_period_end
else current_date end
;

-- add in data from customer table
create table #jca_dm_report_3 as
select a.*,
b.first_order_date,
b.latest_order_date,
b.total_spend,
b.total_orders,
b.orders_in_last_365_days as orders_lm12,
b.small_business_flag,
c.week_in_month,
c.week_in_year,
c.period,
c.month
from #jca_dm_report_2 a
inner join warehouse.d_customers b
on a.retailcustid = b.customer_code
left join warehouse.l_mjw_accounting_dates c
on a.order_date between c.start_date and c.end_date
;



create table #jca_dm_report_4 as
select a.*,
case when b.order_id is not null or c.order_id is not null then 1 else 0
end as incentive_redeemed,
case when b.order_id is not null then 1 else 0
end as incentive_a_redeemed,
case when c.order_id is not null then 1 else 0
end as incentive_b_redeemed
from #jca_dm_report_3 a
left join (select order_id, voucher_barcode 
		from warehouse.d_vouchers_redeemed
		group by order_id, voucher_barcode
		) b
on a.order_id = b.order_id
and a.incentive_barcode::varchar = b.voucher_barcode::varchar
left join (select order_id, voucher_barcode 
		from warehouse.d_vouchers_redeemed
		group by order_id, voucher_barcode
		) c
on a.order_id = c.order_id
and a.incentive_barcode_2::varchar = c.voucher_barcode::varchar
;


-- THIS SHOULD BE TRUNCATE AND INSERT - but is causing errors - not clear why....
-- Switch back to truncate / insert when possible

truncate table majestic_analyst_adh.jca_dm_report_5;

insert into majestic_analyst_adh.jca_dm_report_5 
select *
from  #jca_dm_report_4
;
