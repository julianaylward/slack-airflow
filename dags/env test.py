import os
from dotenv import load_dotenv, find_dotenv
from airflow import DAG

print("Hello")
print(find_dotenv())

dotenv_path = find_dotenv()
load_dotenv(dotenv_path)
print(os.environ['OPEN_WEATHER_API'])

