#Copying Airflow's example of passing variables to a python function from the python operator
# Its extremely hard to find clear documentation on the web for this that actually seems to work!
# Start with the simplest possible use case
#https://stackoverflow.com/questions/50632535/upload-file-to-sftp-using-python
#https://medium.com/@xnuinside/short-guide-how-to-use-postgresoperator-in-apache-airflow-ca78d35fb435

from datetime import datetime, timedelta
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
import psycopg2
from dotenv import find_dotenv, load_dotenv
import os
import pandas as pd
import paramiko

dag = DAG(
    "pass_variable_python_2",
    default_args={
        "owner": "Julian",
        "start_date": days_ago(1),
    },
    schedule_interval='15 16 * * *',
    dagrun_timeout=timedelta(minutes=20)
)

# This actually works!
def fetch_data(ds, **kwargs):
    # Print out the "foo" param passed in via
    # `airflow test example_passing_params_via_test_command run_this <date>
    # -tp '{"foo":"bar"}'`
    print("Starting function")
    print(kwargs["params"]["filename"])
    print(kwargs["params"]["str_sql"])
    source = kwargs["params"]["filename"] + '.csv'
    dotenv_path = find_dotenv()
    load_dotenv(dotenv_path)
    print(os.environ['REDSHIFT_DWH_DATABASE'])
    sql = kwargs["params"]["str_sql"]
    #Get Data:
    con= psycopg2.connect(dbname=os.environ['REDSHIFT_DWH_DATABASE'], host=os.environ['REDSHIFT_DWH_ENDPOINT'], \
        port=os.environ['REDSHIFT_DWH_PORT'],user=os.environ['REDSHIFT_DWH_USERNAME'],password=os.environ['REDSHIFT_DWH_PASSWORD'])
    print("connection established with DWH")
    cur = con.cursor()
    cur.execute(sql) ##e.g. select some data. Triple quotation for multi line strings.
    # Extract the column names
    col_names = []
    for elt in cur.description:
        col_names.append(elt[0])
    df = pd.DataFrame(cur.fetchall(), columns=col_names) ## Fetch the results into an arbitrary data object
    cur.close() ## Close cursor
    con.close() ## close connection
    print("Connection Closed")
    df.to_csv(source,index=False, sep='|', quoting = 1)  ## Conver this to file_name so we can call the function repeatedly
    print("Data saved to file")
    del df #free up memory by clearing df object
    #2: Export Data
    hostname = os.environ['AD_STAG_SFTP_HOST']
    username = os.environ['AD_STAG_SFTP_USER']
    ssh_keyfile = 'adobe_rsa.pem'
    port = os.environ['AD_STAG_SFTP_PORT']
    mykey = paramiko.RSAKey.from_private_key_file(ssh_keyfile)
    print("Opening SFTP connection...")
    destination = '/incoming/from_dwh/' + kwargs["params"]["filename"]  + '_' + datetime.now().strftime("%Y_%m_%d")+'.csv'
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname=hostname,port=port,username=username,pkey=mykey)
    ftp_client=client.open_sftp()
    ftp_client.put(source,destination)
    ftp_client.close()
    print("SFTP Connection Closed")
    #Clean up the file we created
    os.remove(source) #could use xcomm here? Or will need to call repeatedly
    print("CSV File Deleted, task completed")
    return 1 #Pretty sure this isn't necessary


#Initiate with a dummy operator to denote dag commence:
dummy_start = DummyOperator(
    task_id = 'dummy_start_adobe_stag_export_dag',
    dag = dag
    )

#Slack Webhook to notify Majestic BI Team Slack Workspace that the dag is beginning:
slack_start = SlackWebhookOperator(
        task_id = 'slack_start_dag',
        http_conn_id = 'slack_connection',
        message =  'Hello from Airflow! Starting export to Adobe Staging SFTP: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
        channel = '#airflow-monitoring',
        dag = dag
    )
### START THE TRANSFER TASKS BY CALLING PYTHON OPERATOR /function ####

#Fetch stores and pass to Adobe SFTP
fetch_stores = PythonOperator(
    task_id='fetch_stores',
    provide_context=True,
    python_callable=fetch_data,
    params={"filename": "stores","str_sql":"select * \
	from my_schema.my_table"},
    dag=dag,
)

#Fetch products and pass to Adobe SFTP
fetch_products = PythonOperator(
    task_id='fetch_products',
    provide_context=True,
    python_callable=fetch_data,
    params={"filename": "products","str_sql":"select * \
	from my_schema.my_table;"},
    dag=dag,
)

#Fetch order products and pass to Adobe SFTP
fetch_order_products = PythonOperator(
    task_id='fetch_order_products',
    provide_context=True,
    python_callable=fetch_data,
    params={"filename": "order_products","str_sql":"select * \
	from my_schema.my_table"},
    dag=dag,
)

#Fetch orders and pass to Adobe SFTP
#Output at SCV Level
fetch_orders = PythonOperator(
    task_id='fetch_orders',
    provide_context=True,
    python_callable=fetch_data,
    params={"filename": "orders","str_sql":"select * \
	from my_schema.my_table"},
    dag=dag,
)

#Fetch Customers at SCV Level and pass to Adobe SFTP
#Full table refresh
fetch_customers = PythonOperator(
    task_id='fetch_customers',
    provide_context=True,
    python_callable=fetch_data,
    params={"filename": "customers","str_sql":"select * \
	from my_schema.my_table"},
    dag=dag,
)


##### END OF FILE TRANSFER TASKS #########
#Slack Webhook to notify Majestic BI Team Slack Workspace that the dag is ending:
# Backslash is line continuation
slack_end = SlackWebhookOperator(
        task_id = 'slack_end_dag',
        http_conn_id = 'slack_connection',
        message =  'Hello from Airflow! Finished export to Adobe Staging SFTP: {}'.format( \
            datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
        channel = '#airflow-monitoring',
        dag = dag
    )

dummy_end = DummyOperator(
    task_id = 'end_dag',
    dag = dag
    )


dummy_start >> slack_start
slack_start >> fetch_stores
fetch_stores >> fetch_products
fetch_products >> fetch_order_products
fetch_order_products >> fetch_orders
fetch_orders >> fetch_customers
fetch_customers >> slack_end
slack_end >> dummy_end

