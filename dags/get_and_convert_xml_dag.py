#Import libraries
import requests
import pandas as pd
import xml.etree.ElementTree as et
import io
from sqlalchemy import create_engine
from dotenv import load_dotenv, find_dotenv
import psycopg2
from sqlalchemy import create_engine
import os
from datetime import datetime, timedelta
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago

dag = DAG(
    "get_product_xml",
    default_args={
        "owner": "Julian",
        "start_date": days_ago(1),
    },
    schedule_interval='0 */6 * * *',  #every 6 hours
    dagrun_timeout=timedelta(minutes=5)
)

# This actually works!
def fetch_xml(ds, **kwargs):
    #define the URL
    print("Entering function")
    url = kwargs["params"]["url_str"]
    # List the column names we'll need for the DF
    df_cols = ["product_ref", "alcohol_percentage", "bottle_size", "colour","country_of_origin","wine_region",
            "grape_variety","product_image","product_rating","product_rating_buy","number_of_rating","product_url",
            "product_group","product_group_description","product_name","product_type","product_type_glasses_hire",
            "product_type_organic","product_type_vegan","product_type_vegetarian","vintage","web_name",
            "wineify_product_colour","product_description_short","product_description_long","single_bottle_price",
            "mix_six_price","percentage_saving","pound_saving","stock_Europa","low_stock"]
    #get the request
    product_xml = requests.get(url)
    xml = product_xml.text
    print("file collected")
    #Write the file to disk - note sure this is necessary but need to edit the file
    f = open("product_feed_raw.xml", "w",  encoding='utf-8')
    f.write(xml)
    f.close()

    #We need to modify the file, as there is currently some crap at the begining that causes xtree to fail
    i = 0
    txt = 'product_feed_raw.xml' #input file
    tmptxt = 'product_feed.xml' #output file

    with open(tmptxt, 'w',encoding='utf-8') as outfile:
        with open(txt, 'r',encoding='utf-8') as infile:
            for line in infile:
                if i == 0:
                    outfile.write("<channel>")
                    i = i + 1
                else:
                    outfile.write(line)
                continue
    print("xml file first line corrected")
    #parse the xml file
    xtree = et.parse("product_feed.xml")
    xroot = xtree.getroot()
    print("xml file parsed back in")
#Now iterate through the nodes to extract the info into a list of lists
    rows2 = []
    #i = 0
    for node in xroot:
        rows = []
        #print(i)
        for attr in df_cols:
            try:
                element = node.find(attr).text if node is not None else None
                rows.append(element)
            except: #The first 2 elems don't have any elements to find!
                #continue
                element = None
                rows.append(element)
        #i = i + 1
        rows2.append(rows)
    #Convert into pandas
    df = pd.DataFrame(rows2, columns = df_cols)
    print("loaded to pandas")
    #Now Strip out the null values coming from the title elements
    df = df[pd.isna(df['product_ref']) == False]
    #Drop description columns which we already have (and cause upload errors in SQLAlchemy)
    df.drop(['product_description_short','product_description_long'],axis =1,inplace=True)

    #Now load creds for import into the DWH using SQLAlchemy
    dotenv_path = find_dotenv()
    load_dotenv(dotenv_path)

    new_endpoint = os.environ['REDSHIFT_DWH_ENDPOINT']
    new_database = os.environ['REDSHIFT_DWH_DATABASE']
    new_user = os.environ['REDSHIFT_DWH_USERNAME']
    new_password = os.environ['REDSHIFT_DWH_PASSWORD']
    new_port = os.environ['REDSHIFT_DWH_PORT']

    #Upload to new data warehouse: Create the connection
    print("start loading to DWH")
    con = 'postgres+psycopg2://'+new_user+':'+new_password+'@'+new_endpoint+':'+str(new_port)+'/'+new_database
    sql_engine = create_engine(con, echo=False)
    #Now load data using replace if exists
    #set the table name for the uploaded table below:
    table='dim_product_xml'
    df.to_sql(table, con=sql_engine, if_exists='replace', 
                                    schema='master', index=False, method='multi',chunksize = 1000)
    #remove the files
    os.remove(tmptxt)
    os.remove(txt)
    print("Task completed and files removed")


#Slack Webhook to notify Majestic BI Team Slack Workspace that the dag is beginning:
slack_start = SlackWebhookOperator(
        task_id = 'slack_start_dag',
        http_conn_id = 'slack_connection',
        message =  'Airflow: Importing Product XML...: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
        channel = '#airflow-monitoring',
        dag = dag
    )

    #Fetch stores and pass to Adobe SFTP
get_product_xml = PythonOperator(
    task_id='get_product_xml',
    provide_context=True,
    python_callable=fetch_xml,
    params={"url_str": "https://majestorage.blob.core.windows.net/majestorage%5CContent/Images/Uploaded/Xml%5Cxmlfeed.xml"},
    dag=dag,
)

#Slack Webhook to notify Majestic BI Team Slack Workspace that the dag is ending:
# Backslash is line continuation
slack_end = SlackWebhookOperator(
        task_id = 'slack_end_dag',
        http_conn_id = 'slack_connection',
        message =  'Airflow: Successfully Imported Product XML: {}'.format( \
            datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
        channel = '#airflow-monitoring',
        dag = dag
    )

slack_start >> get_product_xml
get_product_xml >> slack_end