#https://medium.com/@xnuinside/short-guide-how-to-use-postgresoperator-in-apache-airflow-ca78d35fb435

import uuid
from datetime import datetime
from airflow import DAG
from airflow.operators.postgres_operator import PostgresOperator
from airflow.utils.trigger_rule import TriggerRule


dag_params = {
    #'owner' : 'Julian',
    #'email': ['julian.aylward@majestic.co.uk'],
    'dag_id': 'PostgresOperator_dag',
    'start_date': datetime(2019, 10, 7),
    'schedule_interval': None
}

with DAG(**dag_params) as dag:

    drop_table_if = PostgresOperator(
        postgres_conn_id= 'original_dwh',
        task_id='drop_table_if_exists',
        sql='''drop table if exists majestic_analyst_adh.jca_new_table;''',
    )

    create_table = PostgresOperator(
        postgres_conn_id= 'original_dwh',
        task_id='create_table',
        sql='''CREATE TABLE majestic_analyst_adh.jca_new_table(
            custom_id integer NOT NULL, timestamp TIMESTAMP NOT NULL, user_id VARCHAR (50) NOT NULL
            );''',
        trigger_rule=TriggerRule.ALL_DONE,
    )

    insert_row = PostgresOperator(
        postgres_conn_id= 'original_dwh',
        task_id='insert_row',
        sql='INSERT INTO majestic_analyst_adh.jca_new_table VALUES(%s, %s, %s)',
        trigger_rule=TriggerRule.ALL_DONE,
        parameters=(uuid.uuid4().int % 123456789, datetime.now(), uuid.uuid4().hex[:10])
    )

    drop_table_if >> create_table
    create_table >> insert_row


