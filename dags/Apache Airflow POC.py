from datetime import timedelta

import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator


#Then set default arguments for the entire workflow
# As a dictionary

default_args = {
    'owner' : 'Julian',
    'start_date' : airflow.utils.dates.days_ago(2),
    'depends_on_past': False,
    'email' : ['julian.aylward@majestic.co.uk'],
    'email_on_failure' : True,
    'email_on_rety' : True,
    'retries' : 1,
    'retry_delay' : timedelta(minutes = 5),
    }



#instantiate a DAG
#Can use cron expressions for schedule interview or @daily, @hourly, @weekly, @monthly, @yearly

dag = DAG(
    'tutorial',
    default_args = default_args,
    description = 'A defo DAG',
    schedule_interval =timedelta(days = 1),
)


#Add tasks

t1 = BashOperator(
    task_id = 'printdate',
    bash_command = 'date',
    depends_on_past = False,
    dag = dag,
    )

t2 = BashOperator(
    task_id = 'sleep',
    bash_command = 'sleep 5',
    depends_on_past = False,
    dag = dag,
    )

templated_command = """
{% for i in range(5)%}
    echo "{{ ds }}"
    echo "{{ macros.ds_add(ds,7) }}"
    echo "{{ params.my_param }}"
{% endfor %}
"""


t3 = BashOperator(
    task_id = 'templated',
    bash_command = templated_command,
    depends_on_past = False,
    params = {'my_param' : 'Parameter I passed in'},
    dag = dag,
    )



#Create dependancies:
#Set upstream or downstream
#e#g#

t1 >> t2
t2 >> t3
#t1 >> [t2,t3] run t2,t3 in parallel
#t1.set_downstream(t2)



