#https://medium.com/@xnuinside/short-guide-how-to-use-postgresoperator-in-apache-airflow-ca78d35fb435

from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.postgres_operator import PostgresOperator
from airflow.utils.trigger_rule import TriggerRule
from airflow.utils.dates import days_ago
from airflow.models import Variable



args  = {
    'owner' : 'Julian',
    'email': ['julian.aylward@majestic.co.uk'],
    'dag_id': 'bi_odwh_master',
    'start_date': days_ago(1),
    'provide_context': True,
    'schedule_interval': '15 10 * * *'
}

tmpl_search_path = Variable.get("sql_path")

dag = DAG(
    'bi_odwh_master',
    schedule_interval="15 10 * * *",
    dagrun_timeout=timedelta(minutes=60),
    template_searchpath=tmpl_search_path,
    default_args=args,
    max_active_runs=1)

bi_jca_odwh_master_start_1 = PostgresOperator(
    postgres_conn_id= 'original_dwh',
    task_id='bi_jca_odwh_master_start_1',
    sql='bi_jca_odwh_master_start_1.sql',
    dag = dag
    )

bi_jca_odwh_master_dm_2 = PostgresOperator(
    postgres_conn_id= 'original_dwh',
    task_id='bi_jca_odwh_master_dm_2',
    sql='bi_jca_odwh_master_dm_2.sql',
    trigger_rule=TriggerRule.ALL_DONE,
    dag = dag
    )

bi_jca_odwh_master_sndshop_3 = PostgresOperator(
    postgres_conn_id= 'original_dwh',
    task_id='bi_jca_odwh_master_sndshop_3',
    sql='bi_jca_odwh_master_sndshop_3.sql',
    trigger_rule=TriggerRule.ALL_DONE,
    dag = dag
    )

bi_jca_odwh_master_retention_4 = PostgresOperator(
    postgres_conn_id= 'original_dwh',
    task_id='bi_jca_odwh_master_retention_4',
    sql='bi_jca_odwh_master_retention_4.sql',
    trigger_rule=TriggerRule.ALL_DONE,
    dag = dag
    )

bi_jca_odwh_master_vchtdr_5 = PostgresOperator(
    postgres_conn_id= 'original_dwh',
    task_id='bi_jca_odwh_master_vchtdr_5',
    sql='bi_jca_odwh_master_vchtdr_5.sql',
    trigger_rule=TriggerRule.ALL_DONE,
    dag = dag
    )

bi_jca_odwh_master_prtoclassn_6 = PostgresOperator(
    postgres_conn_id= 'original_dwh',
    task_id='bi_jca_odwh_master_prtoclassn_6',
    sql='bi_jca_odwh_master_prtoclassn_6.sql',
    trigger_rule=TriggerRule.ALL_DONE,
    dag = dag
    )

bi_jca_odwh_master_trimmed_7 = PostgresOperator(
    postgres_conn_id= 'original_dwh',
    task_id='bi_jca_odwh_master_trimmed_7',
    sql='bi_jca_odwh_master_trimmed_7.sql',
    trigger_rule=TriggerRule.ALL_DONE,
    dag = dag
    )

bi_jca_odwh_master_end_8 = PostgresOperator(
    postgres_conn_id= 'original_dwh',
    task_id='bi_jca_odwh_master_end_8',
    sql='bi_jca_odwh_master_end_8.sql',
    trigger_rule=TriggerRule.ALL_DONE,
    dag = dag
    )

bi_jca_odwh_master_start_1 >> bi_jca_odwh_master_dm_2
bi_jca_odwh_master_start_1 >> bi_jca_odwh_master_sndshop_3
bi_jca_odwh_master_start_1 >> bi_jca_odwh_master_retention_4
bi_jca_odwh_master_start_1 >> bi_jca_odwh_master_vchtdr_5
bi_jca_odwh_master_start_1 >> bi_jca_odwh_master_prtoclassn_6
bi_jca_odwh_master_start_1 >> bi_jca_odwh_master_trimmed_7

bi_jca_odwh_master_dm_2 >> bi_jca_odwh_master_end_8
bi_jca_odwh_master_sndshop_3 >> bi_jca_odwh_master_end_8
bi_jca_odwh_master_retention_4 >> bi_jca_odwh_master_end_8
bi_jca_odwh_master_vchtdr_5 >> bi_jca_odwh_master_end_8
bi_jca_odwh_master_prtoclassn_6 >> bi_jca_odwh_master_end_8
bi_jca_odwh_master_trimmed_7 >> bi_jca_odwh_master_end_8


